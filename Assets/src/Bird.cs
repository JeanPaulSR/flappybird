using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
    public Rigidbody2D rb;

    public float velocidadImpulso;

    public int puntaje;
    public int highScore = 0;

    private Vector3 originalPos;

    public Boolean reset;
    public Boolean awake = false;
    public Boolean dead = false;

    private void Start() {
        rb = GetComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Static;
        originalPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
    }

    private void Update() {
        if (!awake && !dead) {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) || Input.touches.Length > 0)
            {
                rb.bodyType = RigidbodyType2D.Dynamic;
                awake = true;
                rb.velocity = Vector2.up * velocidadImpulso;
            }
        }
        else {
            if(isAlive())
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) || Input.touches.Length > 0)
                {
                    rb.velocity = Vector2.up * velocidadImpulso;
                }
        }  
    }

    public Boolean isAwake()
    {
        return awake;
    }
    public Boolean isAlive()
    {
        return !dead;
    }
    public int getPoints()
    {
        return puntaje;
    }

    public int getHighScore()
    {
        if (reset)
            return highScore;
        return -1;

    }

    public void died()
    {
        rb.bodyType = RigidbodyType2D.Static;
        if (puntaje > highScore)
            highScore = puntaje;
        awake = false;
        reset = true;
        dead = true;
        
    }

    public void restart()
    {
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.transform.position = originalPos;
        puntaje = 0;
        dead = false;
        rb.bodyType = RigidbodyType2D.Static;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public GameObject player;
    public GameObject gameOver;

    private Text scoreTextGO;
    private Text highScoreTextGO;
    private Button restart;
    // Start is called before the first frame update
    void Start()
    {
        scoreTextGO = gameOver.transform.Find("scoreTextGO").GetComponent<Text>();
        highScoreTextGO = gameOver.transform.Find("highScoreTextGO").GetComponent<Text>();
        gameOver.SetActive(false);
        restart = gameOver.transform.Find("Restart").GetComponent<Button>();
        restart.onClick.AddListener(Restart);

    }

    // Update is called once per frame
    void Update()
    {
        if (!player.GetComponent<Bird>().isAlive())
        {
            gameOver.SetActive(true);
            int score = player.GetComponent<Bird>().getPoints();
            scoreTextGO.text = score.ToString();
            int hScore = player.GetComponent<Bird>().getHighScore();
            highScoreTextGO.text = hScore.ToString();

        }


    }

    void Restart()
    {
        player.GetComponent<Bird>().restart();
        gameOver.SetActive(false);



        GameObject[] pipes = GameObject.FindGameObjectsWithTag("Pipe");
        foreach (GameObject pipe in pipes)
        {
            pipe.GetComponent<Tuber�a>().Clean();
        }
        
    }

}

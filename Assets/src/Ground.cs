using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public float velocidad;
    private float currentTime;
    private float timeOfSpawn;
    public GameObject player;

    private void Start()
    {
        currentTime = Time.time;
        timeOfSpawn = currentTime;
        player = GameObject.FindWithTag("Player");
    }

    private void Update()
    {
        if (player.GetComponent<Bird>().isAwake())
        {
            currentTime = Time.time;
            if (timeOfSpawn < currentTime - 12)
                Destroy(this.gameObject);
            transform.position += Vector3.left * velocidad * Time.deltaTime;
        } else
        {
            timeOfSpawn += 0.018f;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        player.GetComponent<Bird>().died();
    }
}

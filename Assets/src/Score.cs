using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{


    public GameObject player;

    private Text scoreText;
    private Text highScoreText;

    private void Awake()
    {
        scoreText = transform.Find("scoreText").GetComponent<Text>();

        highScoreText = transform.Find("highScoreText").GetComponent<Text>();
    }

    private void Update()
    {
        int score = player.GetComponent<Bird>().getPoints();
        scoreText.text = score.ToString();
        int hScore = player.GetComponent<Bird>().getHighScore();
        if (hScore != -1)
            highScoreText.text = hScore.ToString();
        else
        {
            string result = "";
            highScoreText.text = result.ToString();
        }
    }
}

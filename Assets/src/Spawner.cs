using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public bool spawnable;
    public GameObject objectToSpawn;
    public GameObject player;

    private float currentTime;
    private float timeOfLastSpawn = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetComponent<Bird>().isAwake() && player.GetComponent<Bird>().isAlive())
        {
            currentTime = Time.time;
            if (currentTime >= timeOfLastSpawn + 2)
            {
                spawnable = true;
                timeOfLastSpawn = currentTime;

            }
            else
                spawnable = false;

            if (spawnable)
            {
                float range = Random.Range(-1.5f, 2.5f);
                Instantiate(objectToSpawn, transform.position + new Vector3(0, Input.GetAxis("Vertical") + range, 0), transform.rotation);
            }
        }
        else
        {
            currentTime = Time.time;
            timeOfLastSpawn = 0;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2 : MonoBehaviour
{

    public bool spawnable;
    public GameObject objectToSpawn;

    private float currentTime;
    private float timeOfLastSpawn = 0;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = Time.time;
        player = GameObject.FindWithTag("Player");
        Instantiate(objectToSpawn, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {

        if (player.GetComponent<Bird>().isAwake() && player.GetComponent<Bird>().isAlive())
        {
            currentTime = Time.time;
            if (currentTime >= timeOfLastSpawn + 2)
            {
                spawnable = true;
                timeOfLastSpawn = currentTime;

            }
            else
                spawnable = false;

            if (spawnable)
            {
                Instantiate(objectToSpawn, transform.position, transform.rotation);
            }
        }
    }
}

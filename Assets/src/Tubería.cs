using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tubería : MonoBehaviour {

    public float velocidad;
    private float currentTime;
    private float timeOfSpawn;
    public GameObject player;

    private void Start()
    {
        currentTime = Time.time;
        timeOfSpawn = currentTime;
        player = GameObject.FindWithTag("Player");
        StartCoroutine(MueveIzquierda());
    }

    private void Update()
    {

        if (player.GetComponent<Bird>().isAwake())
        {
            currentTime = Time.time;
            if (timeOfSpawn < currentTime - 12)
                Destroy(this.gameObject);
        }
    }

    public IEnumerator MueveIzquierda() {
        while (player.GetComponent<Bird>().isAwake()) {
            transform.position += Vector3.left * velocidad * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.TryGetComponent(out Bird pajaro)) {
            pajaro.puntaje++;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        player.GetComponent<Bird>().died();
    }

    public void Clean()
    {

        Destroy(this.gameObject);
    }
}
